<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
switch ($_SERVER['HTTP_HOST']) {        
    case '192.168.1.104':               
        define('DB_NAME', 'cometdecor');      
        define('DB_USER', 'webuser');
        define('DB_PASSWORD', 'nopassword');
        define('DB_HOST', '192.168.1.200');
        break;
    case 'cometdecor.dd':               
        define('DB_NAME', 'cometdecor');      
        define('DB_USER', 'webuser');
        define('DB_PASSWORD', 'nopassword');
        define('DB_HOST', '192.168.1.200');
        break;
     case 'cometdecor.myanmarcafe.info':
         define('DB_NAME', 'digitald_cometdecor');
         define('DB_USER', 'digitald_webuser');
         define('DB_PASSWORD', '9UrEbO{g0Ck3');
         define('DB_HOST', 'localhost');
         break;
    default :
        define('DB_NAME', 'cometdec_production');
        define('DB_USER', 'cometdec_webuser');
        define('DB_PASSWORD', 'mI=s]nUC^}DO');
        define('DB_HOST', 'localhost');
        break;
}
$protocol = (!empty($_SERVER['HTTPS']) ) ? 'https://' : 'http://';
define('WP_SITEURL', $protocol . $_SERVER['HTTP_HOST']);
define('WP_HOME', $protocol . $_SERVER['HTTP_HOST']);
define('DB_CHARSET', 'utf8');
define('DB_COLLATE', '');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Fq~Y$! 8E@<4:s,/~}9Z/2+1jpD{-b_(*syQ?z!U59t8)j}qSEt}mVc;PD%YbF(<');
define('SECURE_AUTH_KEY',  'P9B[`W(ob8Y3@My>[[bnyj>/&tVz*uTS``THUY,,yvCbi2flsV1I5GW<BkI>-1[f');
define('LOGGED_IN_KEY',    'JHseBIRg4oS&&-3S?*AhehI8}!h(Qn|hW?0H0{l|BGn&7Do<=wg?@i0Nk50Ia9~t');
define('NONCE_KEY',        't2{Q641Oe],N(sI>PX:D:m!cVFq>b0v9b(S-Qf0c`_azZBKJ[u8uD,x)2BH`I{>k');
define('AUTH_SALT',        'iMsKdDena*eO[IdjEhv2x?fP%v(o%%}g]$mP&dWi!CaL^!XGRgL=q*U!n-RBuHD}');
define('SECURE_AUTH_SALT', '!>QiA<tiR=cDuET_t9kXx`pi=;iPU5b{3Z_MzCm}A/j}6Jza?|~{Hkzg/4.65A[q');
define('LOGGED_IN_SALT',   'EU>,hJWaPt=z5{CL3:)Q^{/3O(jgv=mm|j*FcrBv$?ab73rem!u+KBK{`twC_>p;');
define('NONCE_SALT',       '%pEYUS&teQgb%`8(|4)+mZS(kXdWe()X<>Y[BJ}g?(58a>C!#6=[pje}g2&{fA<3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cd_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */
define('AUTOSAVE_INTERVAL', 180);  
define( 'WP_AUTO_UPDATE_CORE', false);
define( 'WP_POST_REVISIONS', 3 );

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
