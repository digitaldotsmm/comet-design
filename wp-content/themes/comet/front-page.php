<?php get_header(); ?>
<div class="homewrap">
	
	<?php echo do_shortcode('[soliloquy id="29"]');?>
	
	<div class="welcomemsg">
		<h1 class="underline">Comet Design & Decoration</h1>
		<div class="welcomeexcept">
                    <?php echo $post->post_content; ?>
		</div>
	</div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
