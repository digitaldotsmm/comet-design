<?php
define('TEMPLATE_URL', get_stylesheet_directory_uri());
define('ASSET_URL', TEMPLATE_URL . '/assets/');

define('ASSET_VERSION', 1);

define('CD_TYPE_PROJECT', 'project');
define('CD_TYPE_PORFOLIO', 'portfolio');
define('CD_TYPE_PRODUCT', 'product');
define('CD_TERM_PRODUCT', 'product-category');
define('CD_TYPE_COSTUME_FURNITURE', 'custom-made-furniture');
define('CD_TYPE_DECORATIVE_PANEL', 'decorative-panels');

require_once('includes/aq_resizer.php');

#######################################################
/* * ********* CSS merge and minify process *********** */
#######################################################

if ($_SERVER['HTTP_HOST'] == 'cometdecor.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
    require( 'includes/class.magic-min.php' );
    $minified = new Minifier(
            array(
        'echo' => false
            )
    );
    //exclude example
    $css_exclude = array(
            //TEMPLATEPATH . '/assets/css/bootstrap.css', 
            //TEMPLATEPATH . '/assets/css/flexslider.css'
    );
    //order example
    $css_order = array(
            //TEMPLATEPATH . '/assets/css/reset.css',     
    );

    $minified->merge(TEMPLATEPATH . '/assets/css/combine.min.css', TEMPLATEPATH . '/assets/css/combine', 'css', $css_exclude, $css_order);
}

function the_template_url() {
    echo TEMPLATE_URL;
}

################################################################################
// Enqueue Scripts
################################################################################

function init_scripts() {
    wp_deregister_script('wp-embed');
    wp_deregister_script('jquery');
    wp_deregister_script('comment-reply');
}

function add_scripts() {
    $js_path = ASSET_URL . 'js';
    $css_path = ASSET_URL . 'css';
    if ($_SERVER['HTTP_HOST'] == 'cometdecor.dd' || strpos($_SERVER['HTTP_HOST'], '192') !== false) {
        $js_libs = array(
            array(
                'name' => 'jquery',
                'src' => $js_path . '/jquery.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'bootstrap',
                'src' => $js_path . '/bootstrap.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'flexslider',
                'src' => $js_path . '/jquery.flexslider.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'prettyphoto',
                'src' => $js_path . '/jquery.prettyphoto.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'script',
                'src' => $js_path . '/script.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'google map',
                'src' => '//maps.googleapis.com/maps/api/js?key=AIzaSyAKhMQvkXWDu3FlOYeZAT7hzWt5U4YRQrM',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => false
            ),         
        );

        $css_libs = array(
            array(
                'name'  => 'bootstrap',
                'src'   => $css_path . '/bootstrap.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            array(
                'name'  => 'font Lato',
                'src'   => '//fonts.googleapis.com/css?family=Lato',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            
            array(
                'name' => 'font-awesomes',
                'src' => $css_path . '/font-awesome.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),            
            array(
                'name' => 'style',
                'src' => TEMPLATE_URL . '/style.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'screen'
            ),
            
        );
    } else {
        $js_libs = array(
            array(
                'name' => 'jquery',
                'src' => '//cdn.jsdelivr.net/jquery/2.1.1/jquery.min.js',
                'dep' => null,
                'ver' => null,
                'is_footer' => false
            ),
            array(
                'name' => 'bootstrap',
                'src' => '//cdn.jsdelivr.net/bootstrap/3.3.6/js/bootstrap.min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'flexslider',
                'src' => '//cdn.jsdelivr.net/flexslider/2.2.2/jquery.flexslider-min.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'prettyphoto',
                'src' => $js_path . '/jquery.prettyphoto.js',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => true
            ),
            array(
                'name' => 'google map',
                'src' => '//maps.googleapis.com/maps/api/js?key=AIzaSyAKhMQvkXWDu3FlOYeZAT7hzWt5U4YRQrM',
                'dep' => 'jquery',
                'ver' => null,
                'is_footer' => false
            ),    
            // array(
            //     'name' => 'plugins',
            //     'src' => $js_path . '/plugins.js',
            //     'dep' => 'jquery',
            //     'ver' => null,
            //     'is_footer' => true
            // ),
            array(
                'name' => 'script',
                'src' => $js_path . '/script.js',
                'dep' => 'jquery',
                'ver' => ASSET_VERSION,
                'is_footer' => true
            ),
        );

        $css_libs = array(
            array(
                'name'  => 'bootstrap',
                'src'   => '//cdn.jsdelivr.net/bootstrap/3.3.6/css/bootstrap.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            array(
                'name'  => 'font Lato',
                'src'   => '//fonts.googleapis.com/css?family=Lato',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),
            
            array(
                'name' => 'font-awesomes',
                'src' => '//cdn.jsdelivr.net/fontawesome/4.6.1/css/font-awesome.min.css',
                'dep'   => null,
                'ver'   => null,
                'media' => 'screen'
            ),            
            array(
                'name' => 'style',
                'src' => TEMPLATE_URL . '/style.css',
                'dep'   => null,
                'ver'   => ASSET_VERSION,
                'media' => 'screen'
            ),
        );
    }
    foreach ($js_libs as $lib) {
        wp_enqueue_script($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['is_footer']);
    }
    foreach ($css_libs as $lib) {
        wp_enqueue_style($lib['name'], $lib['src'], $lib['dep'], $lib['ver'], $lib['media']);
    }
}

function my_deregister_scripts() {
    global $post_type;

    if (!is_front_page() && !is_home()) {
        
    }
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

function my_deregister_styles() {
    global $post_type;
    if (!is_page('contact-us')) { //only include in contact us page
    }
}

if (!is_admin())
    add_action('init', 'init_scripts', 10);
add_action('wp_enqueue_scripts', 'add_scripts', 10);
add_action('wp_enqueue_scripts', 'my_deregister_scripts', 100);
add_action('wp_enqueue_scripts', 'my_deregister_styles', 100);



################################################################################
// Add theme support
################################################################################

add_theme_support('automatic-feed-links');
add_theme_support('nav-menus');
add_post_type_support('page', 'excerpt');
add_theme_support('post-thumbnails', array('post', 'page', 'portfolio', 'product'));

if (function_exists('register_nav_menus')) {
    register_nav_menus(
            array(
                'main' => 'Main',
                'helpful' => 'Helpful Info',
                'privacy' => 'Privacy Info',
                'menu-left' => 'Left Menu',
                'menu-right' => 'Right Menu',
            )
    );
}

################################################################################
// Add theme sidebars
################################################################################

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => __('Main - Sidebar'),
        'id' => 'main-sidebar-widget-area',
        'description' => 'Widgets in this area will be shown on the right sidebar of default page',
        'before_widget' => '<aside class="widget">',
        'after_widget' => '</aside>',
        'before_title' => '',
        'after_title' => '',
    ));
//    register_sidebar(array(
//        'name' => __('Page - Sidebar'),
//        'id' => 'page-widget-area',
//        'description' => 'Widgets in this area will be shown on the right sidebar of pages',
//        'before_widget' => '<aside class="widget">',
//        'after_widget' => '</aside>',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Newsletter - Sidebar'),
//        'id' => 'footer-newsletter-widget-area',
//        'description' => 'Widgets in this area will be shown on the top of footer',
//        'before_widget' => '<div class="row" id="newsletter-form-container">',
//        'after_widget' => '</div>',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Left - Sidebar'),
//        'id' => 'footer-left-widget-area',
//        'description' => 'Widgets in this area will be shown on the left-hand side of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Middle - Sidebar'),
//        'id' => 'footer-middle-widget-area',
//        'description' => 'Widgets in this area will be shown in the middle of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
//    register_sidebar(array(
//        'name' => __('Footer Right - Sidebar'),
//        'id' => 'footer-right-widget-area',
//        'description' => 'Widgets in this area will be shown on the right-hand side of footer',
//        'before_widget' => '',
//        'after_widget' => '',
//        'before_title' => '',
//        'after_title' => '',
//    ));
}





################################################################################
// Comment formatting
################################################################################

function theme_comments($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment;
    ?>
    <li>
        <article <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
            <header class="comment-author vcard">
    <?php echo get_avatar($comment, $size = '48', $default = '<path_to_url>'); ?>
    <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
                <time><a href="<?php echo htmlspecialchars(get_comment_link($comment->comment_ID)) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(), get_comment_time()) ?></a></time>
                <?php edit_comment_link(__('(Edit)'), '  ', '') ?>
            </header>
                <?php if ($comment->comment_approved == '0') : ?>
                <em><?php _e('Your comment is awaiting moderation.') ?></em>
                <br />
            <?php endif; ?>

    <?php comment_text() ?>

            <nav>
            <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </nav>
        </article>
        <!-- </li> is added by wordpress automatically -->
                <?php
            }


################################################################################
// Pagination
################################################################################
if ( ! function_exists( 'dd_pagination' ) ) :
    function dd_pagination() {
        global $wp_query;

        $big = 999999999; // need an unlikely integer
        
        echo paginate_links( array(
            'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'format'    => '?paged=%#%',
            'current'   => max( 1, get_query_var('paged') ),
            'total'     => $wp_query->max_num_pages,
                        'show_all'  => false,
                        'end_size'  => 1,
                        'mid_size'  => 2,
                        'prev_next' => true,
                        'prev_text' => __('First'),
                        'next_text' => __('Last'),
                ) );
    }
endif;

//Over Wride post per page

function product_archive($query) {     
    if($query->is_main_query() && !is_admin()){
        if (is_post_type_archive(CD_TYPE_PORFOLIO)){
             $query->set('posts_per_page', '20');
        }
    }
}

add_action('pre_get_posts', 'product_archive');




function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}
 
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );
            
