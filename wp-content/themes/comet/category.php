<?php get_header(); ?>
<div id="category">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="pagetitle text-uppercase">News</h1>
			</div>
		</div>
	</div>
	<div class="container">
	    <div class="row">
			<div class="col-md-12 margintop50 marginbottom50"> 
				<?php $count=1; ?>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				<?php 
					$img_url = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
                    $post_image = aq_resize($img_url[0], 500, 300, true, true, true);
				?>
				<?php if($count%2){ ?>
				
				<section class="newsection clearfix">
					<div class="row">
						<div class="col-md-6">
							<div class="newswrap">
								<a href="<?php the_permalink(); ?>">
									<img src="<?php echo $post_image; ?>" class="img-responsive img-thumbnail" alt="<?php the_title(); ?>">
								</a>
							</div>
						</div>
						<div class="col-md-6">
							<div class="news-header">
		                        <a href="<?php the_permalink(); ?>">
		                            <h4 class="text-uppercase"><?php the_title(); ?></h4>
		                        </a>
		                        <span class="news-date"><?php the_time('M j, Y'); ?></span>
		                    </div>
                            <div class="news-intro">
		                        <p><?php echo $post->post_excerpt; ?></p>
		                        <a href="<?php the_permalink(); ?>"><span class="news-readmore">Read More</span></a>
		                    </div>
						</div>
					</div>					
				</section>

				<?php }else{ ?>

				<section class="newsection clearfix">
					<div class="row">
					<div class="col-md-6 pull-right">
						<div class="newswrap">
							<a href="<?php the_permalink(); ?>">
								<img src="<?php echo $post_image; ?>" class="img-responsive img-thumbnail"  alt="<?php the_title(); ?>">
							</a>
						</div>
					</div>
					<div class="col-md-6 pull-left">
						<div class="news-header">
	                        <a href="<?php the_permalink(); ?>">
	                            <h4 class="text-uppercase"><?php the_title(); ?></h4>
	                        </a>
	                        <span class="news-date"><?php the_time('M j, Y'); ?></span>
	                    </div>
                        <div class="news-intro">
	                        <p><?php echo $post->post_excerpt; ?></p>
	                        <a href="<?php the_permalink(); ?>"><span class="news-readmore">Read More</span></a>
	                    </div>
					</div>
					</div>
				</section>

				<?php }$count++; ?>


				<?php endwhile; ?>
				<!-- <div id="pagination" class="margintop20 margin-bottom-20">
	                <?php dd_pagination(); ?>
	            </div> -->
        		<?php endif; ?>
			</div>
	    </div>
	</div>
</div>
<?php get_footer(); ?>
