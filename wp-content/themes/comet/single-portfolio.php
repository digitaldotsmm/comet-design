<?php get_header() ?>
<?php 
	$portfoimg_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'full');
	$portfo_img = aq_resize($portfoimg_url,700,500,true,true,true); 
	$prodgalleries = get_field('gallery',$post->ID);
?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="pagetitle text-uppercase"><?php the_title() ?></h1>
			</div>
		</div>
	</div>
<div id="sgportfolio">
	<div class="container">
		<div class="row margintop30 marginbottom30" >
			
		<?php if ($prodgalleries): ?>
			<?php foreach ($prodgalleries as $gallery) {
				$portfo_img = aq_resize($gallery['url'],377,377,true,true,true); 
				echo '<div class="col-md-3 col-lg-3 col-xs-12 col-sm-6 marginbottom20"><div class="text-center bigeffect"><a href="'. $gallery['url'] .'" rel="prettyPhoto" ><img src="'. $portfo_img .'" alt="Comet Decoration & Design '. $post->post_title . '" /></a></div></div>';
			} ?>
		<?php endif ?>

		</div>
	</div>
</div>						

<?php get_footer() ?>