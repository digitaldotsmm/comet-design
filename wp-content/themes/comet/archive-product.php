<?php get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="pagetitle text-uppercase">Products</h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row margintop50 marginbottom50">
			<?php 
				if (have_posts()): while (have_posts()):the_post();
				$img = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full');
				$prod_image = aq_resize($img[0], 270, 270, true, true, true);
			?>
			
			<div class="col-xs-12 col-sm-6 col-lg-3 col-md-3 marginbottom30 text-center">
				<div class="productwrap">
					<img src="<?php echo $prod_image; ?>" alt="<?php echo $post->post_title; ?>" title="<?php echo $post->post_title; ?>">
					<h2 class="producttitle"><?php echo $post->post_title; ?>asdfasdfasdf</h2>
					
					<!-- <a href="<?php echo $link; ?>"><img src="<?php echo $prod_image; ?>" alt="<?php echo $post->post_title; ?>" title="<?php echo $post->post_title; ?>"></a>
					<a href="<?php echo $link; ?>"><div class="producttitle"><?php echo $post->post_title; ?></div></a> -->
				</div>
			</div>
			<?php endwhile;endif; ?>
		</div>
	</div>
<?php get_footer(); ?>
