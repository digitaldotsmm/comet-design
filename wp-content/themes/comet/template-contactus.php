<?php /* Template Name: Contact us */ ?>
<?php get_header() ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="pagetitle text-uppercase"><?php the_title() ?></h1>
			</div>
		</div>
		<div class="row margintop50">
			<div class="col-md-6">
				<h2 class="tblock-title">GET IN TOUCH</h2>
				<!-- <p style="font-size:18px;font-family:Lato;font-weight:600;">Please fill in your details and we will be in contact with you shortly. </p> -->
				<?php echo do_shortcode('[contact-form-7 id="28" title="Contact form 1"]');?>
			</div>
			<div class="col-md-6" >
				<h2 class="tblock-title">CONTACT ADDRESS</h2>
				<ul class="list-icons list-unstyled">
					<?php if($THEME_OPTIONS['info_address']): ?>
						<li><i class="fa fa-map-marker"></i><?php echo $THEME_OPTIONS['info_address']; ?></li>
					<?php endif; ?>
                	<?php if($THEME_OPTIONS['info_hotline']): ?>
						<li><i class="fa fa-phone"></i>Phone: <?php echo $THEME_OPTIONS['info_hotline']; ?></li>
					<?php endif; ?>
					<?php if($THEME_OPTIONS['info_email']): ?>
						<li><i class="fa fa-envelope"></i>Email: <a href="mailto:<?php echo $THEME_OPTIONS['info_email']; ?>"><?php echo $THEME_OPTIONS['info_email']; ?></a></li>
					<?php endif; ?>
					<?php if($THEME_OPTIONS['info_website']): ?>
						<li><i class="fa fa-globe"></i>Website: <?php echo $THEME_OPTIONS['info_website']; ?></li>
					<?php endif; ?>
				</ul>

				<h5 class="tblock-socialtitle">FOLLOW US</h5>
				<div id="contactaddid">
					<ul class="dt-sc-social-icons ">
					<?php if($THEME_OPTIONS['facebookid']): ?>
						<li class="facebook"><a href="<?php echo $THEME_OPTIONS['facebookid']; ?>" target="_blank" title="facebook" class="facebook"><i class="fa fa-facebook"></i></a></li>
					<?php endif; ?>
					<?php if($THEME_OPTIONS['twitterid']): ?>
						<li class="twitter"><a href="<?php echo $THEME_OPTIONS['twitterid']; ?>" target="_blank" title="twitter" class="twitter"><i class="fa fa-twitter "></i></a></li>
					<?php endif; ?>
					<?php if($THEME_OPTIONS['instagramid']): ?>
						<li class="instagram"><a href="<?php echo $THEME_OPTIONS['instagramid']; ?>" target="_blank" title="instagram" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					<?php endif; ?>
					</ul>
				</div>
				<br>
				
			</div>
			<div class="col-md-12">
				<h2 class="maptitle">Office Location</h2>
				<div class="margintop20 marginbottom50">
					<div id="googleMap" style="width:100%;height:400px;"></div>
				</div>
				
			</div>
			
		</div>
	</div>


<script type="text/javascript">
	$(document).ready(function(){

		var mycenter = new google.maps.LatLng(16.785029,96.155150);
		function initMap() {
					
			map = new google.maps.Map(document.getElementById('googleMap'), {
				center: mycenter,
				zoom: 17,
				mapTypeId:google.maps.MapTypeId.ROADMAP
			});

			marker=new google.maps.Marker({
	            position: mycenter,
				map: map,
				animation: google.maps.Animation.DROP,
				title: "Comet Decoration & Design"
			});
			  
			var contentString = '<div id="infowindow">'+' <div class="text-center marginbottom20"><img src="<?php echo $THEME_OPTIONS['logo'] ?>" alt="Comet Design & Decoration Logo" /></div>'+'<div id="bodyinfowindow">'+
			'<?php if($THEME_OPTIONS['info_address']): ?><p><i class="fa fa-map-marker"></i> : <?php echo $THEME_OPTIONS['info_address'];?></p><?php endif; ?>'+
			'<?php if($THEME_OPTIONS['info_hotline']): ?><p><i class="fa fa-phone"></i> : <?php echo $THEME_OPTIONS['info_hotline'];?></p><?php endif; ?>'+
			'<?php if($THEME_OPTIONS['info_email']): ?><p><i class="fa fa-envelope"></i> : <?php echo $THEME_OPTIONS['info_email'];?></p><?php endif; ?>'+
			'<?php if($THEME_OPTIONS['info_website']): ?><p><i class="fa fa-globe"></i> : <?php echo $THEME_OPTIONS['info_website'];?></p><?php endif; ?>'+
			'</div>'+'</div>';

			var infowindow = new google.maps.InfoWindow({
          		content: contentString
        	});

			marker.setMap(map);
			marker.addListener('click', function() {
          		infowindow.open(map, marker);
        	});
		}

    	google.maps.event.addDomListener(window, 'load', initMap);
    });
</script>
<?php get_footer() ?>