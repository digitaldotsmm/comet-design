<?php get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="pagetitle text-uppercase"><?php the_title() ?></h1>
			</div>
		</div>
	</div>
	<div class="contentwrap">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php echo apply_filters("the_content",$post->post_content); ?>
				</div>
		    </div>
	   </div>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>

