</div><!-- #main -->
<div class="cleared"></div>
<?php global $THEME_OPTIONS; ?>
<?php if (!is_front_page()) { ?>
<div class="innerfooter">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-lg-3 col-md-3">
				<?php if($THEME_OPTIONS['info_address']): ?>
					<div class="innfooterinfowrap">
						<div class="text-center"><i class="fa fa-map-marker"></i></div>
						<div class="text-center margintop10"><?php echo $THEME_OPTIONS['info_address']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-3">
				<?php if($THEME_OPTIONS['info_hotline']): ?>
					<div class="innfooterinfowrap">
						<div class="text-center"><i class="fa fa-phone"></i></div>
						<div class="text-center margintop10"><?php echo $THEME_OPTIONS['info_hotline']; ?></div>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-3">
				<?php if($THEME_OPTIONS['info_email']): ?>
					<div class="innfooterinfowrap">
						<div class="text-center"><i class="fa fa-envelope"></i></div>
						<div class="text-center margintop10"><a href="mailto:<?php echo $THEME_OPTIONS['info_email']; ?>"><?php echo $THEME_OPTIONS['info_email']; ?></a></div>
					</div>
				<?php endif; ?>
			</div>
			<div class="col-md-3">
				<div class="text-center">
				<ul class="">
				<?php if($THEME_OPTIONS['facebookid']): ?>
					<li class="facebook"><a href="<?php echo $THEME_OPTIONS['facebookid']; ?>" target="_blank" title="facebook" class="facebook"><i class="fa fa-facebook"></i></a></li>
				<?php endif; ?>
				<?php if($THEME_OPTIONS['twitterid']): ?>
					<li class="twitter"><a href="<?php echo $THEME_OPTIONS['twitterid']; ?>" target="_blank" title="twitter" class="twitter"><i class="fa fa-twitter "></i></a></li>
				<?php endif; ?>
				<?php if($THEME_OPTIONS['instagramid']): ?>
					<li class="instagram"><a href="<?php echo $THEME_OPTIONS['instagramid']; ?>" target="_blank" title="instagram" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				<?php endif; ?>
				</ul>
				<div class="text-center">Follow us</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<?php } ?>
<div id="footer">
	
	<div class="leftfooter">Copyrights <?php echo date('Y'); ?> &COPY; <a href="/">Global Comet Group co.,LTD.</a>aLL rights Reserved</div>
	<div class="rightfooter">Developed by <a href="http://www.digitaldots.com.mm" target="_blank"><img src="<?php echo ASSET_URL; ?>images/digitaldots.png" alt="Web Development in Yangon: Digital Dots" class="ddlogo"></a></div>
</div>				
<?php wp_footer(); ?>
</body>
</html>	