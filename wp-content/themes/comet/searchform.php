<div class="module module-header-tools">
	<form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get">
		<fieldset class="form form-search-mini">
			<ul>
				<li>
					<label for="kwd" class="hidden">Search</label>
					<input type="text" name="s" id="s" class="input-132 clearInput " Placeholder="SEARCH " value="" title="Site search"/><i class="icosearch fa fa-search fa-lg" aria-hidden="true"></i>
				</li>
				
			</ul>
		</fieldset>
	</form>
</div>
<!-- /module module-nav rounded-4 --> 
