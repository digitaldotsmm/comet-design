<?php get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
				<h1 class="pagetitle text-uppercase">Portfolio</h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row margintop50 marginbottom50">
			<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
				<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
				
				<?php if(have_posts()): $j=1; while(have_posts()): the_post(); ?>
				<?php 
					if (strlen($post->post_excerpt) > 100) {
						$excerpt = substr($post->post_excerpt, 0, 100)."..."; 
					} else {
						$excerpt = $post->post_excerpt;
					}
					$porfolink = get_permalink();
	                $galleries = get_field('gallery',$post->ID,TRUE);
	            ?>
					<div class="col-xs-12 col-sm-6 col-lg-3 col-md-3 text-center paddingnone">
						<div class="imgwrapper">
	                        <?php if($galleries): $i=1; foreach($galleries as $gallery): ?>
	                            <a href="<?php echo $gallery['url']; ?>" rel="prettyPhoto[pp_gal<?php echo $j; ?>]" title="<?php the_title(); ?>">
	                            <?php $porfolio_image = aq_resize($gallery['url'], 360, 360, true, true, true);  ?>
	                                <img src="<?php echo $porfolio_image; ?>" class="img-responsive" style="<?php echo($i!=1)? 'display:none':''; ?>">
	                            </a>
	                        <?php $i++; endforeach; endif; ?>
							<h2 class="prodtitle"><?php echo $post->post_title; ?></h2>
						</div>
					</div>
				<?php $j++; endwhile; ?>
					<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
						<div id="pagination" class="margintop20 margin-bottom-20">
			                <?php dd_pagination(); ?>
			            </div>
					</div>
	            <?php endif; ?>
	        </div>
		</div>
	</div>
<?php get_footer(); ?>

