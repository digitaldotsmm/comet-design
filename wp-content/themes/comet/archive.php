<?php get_header();$product_type = get_queried_object(); //var_dump($product_type); ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
				<h1 class="pagetitle text-uppercase"><?php echo $product_type->name; ?></h1>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row margintop50 marginbottom50">

			<?php
	            $args = array(
	                'post_type' => CD_TYPE_PRODUCT,
	                'posts_per_page' => -1,
	                'post_status' => 'publish',
	                'tax_query' => array(
	                    array(
	                    'taxonomy' => CD_TERM_PRODUCT,
	                    'field' => 'id',
	                    'terms' => $product_type->term_id
	                     )
	                  )
	                );
	            $products = get_posts($args);
	        ?>
	        
			<?php 
				$count=1;

				foreach ($products as $product) :
				
				$img = wp_get_attachment_image_src(get_post_thumbnail_id($product->ID), 'full');
				$prod_image = aq_resize($img[0], 270, 270, true, true, true);
				$link = get_permalink($product->ID);
				if ($count == 1 || $count%4 == 1) { echo "<div class='row'>"; } 
			?>
			
			<div class="col-xs-12 col-sm-6 col-lg-3 col-md-3 marginbottom30 text-center">
				<div class="productwrap">
					<a href="<?php echo $link; ?>"><img src="<?php echo $prod_image; ?>" alt="<?php echo $product->post_title; ?>" title="<?php echo $product->post_title; ?>"></a>
					<a href="<?php echo $link; ?>"><h2 class="producttitle"><?php echo $product->post_title; ?></h2></a>
				</div>
			</div>
			<?php if ($count%4 == 0 || $tour_count == $count) { echo "</div>"; } ?>
			<?php $count++;endforeach; ?>
			
		</div>
	</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
