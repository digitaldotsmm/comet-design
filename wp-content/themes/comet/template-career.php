<?php /* Template Name: Career */ ?>

<?php get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 class="pagetitle text-uppercase"><?php the_title() ?></h1>
			</div>
		</div>
	</div>
	<div class="contentwrap">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php echo apply_filters("the_content",$post->post_content); ?>
					<hr>
				</div>

				<div class="col-md-12">
					<h2 class="careertitle">Career Submission Form</h2>
					<div><p>Please complete and submit the form below to apply for a position at Comet Decoration & Design.</p></div><br>
					<div class="row">
						<div class="col-md-6">
							<div id="careerform">
							  	<?php echo do_shortcode('[contact-form-7 id="283" title="career form"]');?>
							</div>
						</div>
					</div>
				</div>	
		    </div>
	   </div>
	</div>


<?php get_footer(); ?>

