<?php global $THEME_OPTIONS; ?>
<!doctype html>
<!--[if lt IE 7 ]>  <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>     <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>     <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>     <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="ltr" lang="en"  class="no-js">
<!--<![endif]-->
<head>
<meta charset="UTF-8">
<title><?php wp_title(''); ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
<?php if ( file_exists(TEMPLATEPATH .'/favicon.png') ) : ?>
<link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/favicon.png">
<?php endif; ?>
<!--[if lt IE 9]>
<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<?php global $THEME_OPTIONS; $body_classes = join( ' ', get_body_class() ); ?>
<body class="<?php if( !is_search() )echo $body_classes; ?>">

<div id="header">
    <div class="main-header clearfix">
        <div class="header-left col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <span class="hot-line">
                <i class="fa fa-phone fa-lg"></i>
                <?php echo $THEME_OPTIONS['info_hotline']; ?>
            </span>
        </div><!-- end of header-left -->
        <div class="logo-wrapper col-xs-12 col-sm-4 col-md-4 col-lg-4 text-center">
            <a href="<?php echo WP_HOME; ?>">
                <img src="<?php echo $THEME_OPTIONS['logo'] ?>" alt="Comet Design & Decoration Logo" />                
            </a>
        </div>
        <div class="header-right col-xs-12 col-sm-4 col-md-4 col-lg-4">
            <div class="social-wrapper">
                <?php if( $THEME_OPTIONS['facebookid'] ): ?>
                    <a href="<?php echo $THEME_OPTIONS['facebookid']; ?>" target="_blank" title="Like us on Facebook">
                        <i class="fa fa-facebook-official fa-lg" aria-hidden="true"></i>
                    </a>
                <?php endif; ?>
                        
                <?php if( $THEME_OPTIONS['twitterid'] ): ?>
                    <a href="<?php echo $THEME_OPTIONS['twitterid']; ?>" target="_blank" title="Follow us on Facebook">
                        <i class="fa fa-twitter-square fa-lg" aria-hidden="true"></i>
                    </a>
                <?php endif; ?>
            </div> <!-- end of socialwrapper -->
            <div class="search-box">
                <?php get_search_form(); ?>
            </div> <!-- end of search box -->
        </div> <!-- end of header-right -->
    </div> <!-- End .main-header -->
    <div class="nav-wrapper">
        <div class="container">
            <nav class="" style="display: table;">
                <?php
                    wp_nav_menu(array(
                        'theme_location' => 'main',
                        'container_id' => 'menu-right',
                        'menu_class' => 'nav navbar-nav',                                
                        ));
                ?>   
            </nav>
            <div class="visible-xs">
                <!--  mobile menu  -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-menu" aria-expanded="false">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse" id="mobile-menu">
                    <?php
                        wp_nav_menu(array(
                            'container' => 'div',
                            'theme_location' => 'main',
                            'container_class' => 'mobile-menubar',
                            'menu_class' => 'nav navbar-nav'
                        ));
                    ?>
                </div>
            </div>
        </div> <!-- End .container -->
    </div> <!-- End .nav-wrapper -->
    <?php if(!is_front_page()): ?>
        <?php if( $THEME_OPTIONS['Bannerlogo'] ): ?>
        <div class="banner">
            <img src="<?php echo $THEME_OPTIONS['Bannerlogo'];  ?>">
        </div>
        <?php endif ?>
    <?php endif ?>
</div><!--/header -->
<div id="main" class="clearfix">
    <div id="content">
