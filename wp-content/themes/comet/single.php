<?php get_header(); ?>
<div class="module">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	<h1><?php the_title() ?></h1>
	<div class="module-content">
		<p>Posted by:
			<?php if ( $author = get_custom_post_meta( "post_author" ) ): ?>		
				<?php echo $author; ?>
			<?php else: ?>
				<?php the_author() ?>
			<?php endif; ?>
		</p>
		<p> Posted:
			<?php the_time('d F Y') ?>
		</p>
		<?php //include_once('includes/social-module.php') ?>
		<?php the_content(''); ?>
	</div>
	<?php the_tags('<div class="module">Tags: ', ' ', '</div>'); ?>			
	<?php  comments_template(); ?>
	<?php endwhile; endif; ?>	
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
