<?php get_header(); ?>
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12">
				<h1 class="pagetitle text-uppercase"><?php the_title() ?></h1>
			</div>
		</div>
	</div>
	
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 text-center">
					<img src="<?php echo ASSET_URL; ?>images/404.png" alt="" title="" class="img-responsive img404">
					<div><a href="/" title="Back to site" class="error-back">Go To Home</a></div>
				</div>
		    </div>
	   </div>
	


<?php get_footer(); ?>

