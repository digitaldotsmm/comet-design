<?php 
    get_header(); 
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="pagetitle text-uppercase">
                    <?php echo("Search Result"); ?>
                </h1>
            </div>
        </div>
    </div>
    <div class="contentwrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>	
                        <h4>
                            <a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h4>
                        <?php if(the_content()): ?>
                			<p><?php echo string_limit_words(strip_tags($post->post_content, 30)) . '...'; ?></p>
                        <?php endif; ?>
                        <?php endwhile; else: ?>
			             <p style="color:red;"><?php _e('Sorry, no posts matched your criteria.'); ?></p>
		              <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>